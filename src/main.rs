use lazy_static::*;
use minifb::*;
use rand::seq::SliceRandom;
use rand::*;
use rayon::prelude::*;
use std::ops::*;
use std::sync::Mutex;

//use std::sync::{mpsc::channel, Arc, Mutex};
//use rayon::*;

const BLOOD: &[u32] = &[0x00630f0f, 0x008a0202, 0x00a41313, 0x00c01010, 0x00d90000];
const ROCKS: &[u32] = &[0x00686868, 0x00808080, 0x008f8f8f];
//const DEEP_ROCKS: &[u32] = &[0x0006050a, 0x00171322, 0x002e2542, 0x00171322];
const HILLS: &[u32] = &[0x0037705A, 0x002c644e, 0x002d8160];
const COLORS: &[u32] = &[0x0037705A, 0x00EADFA0, 0x00F7821A, 0x00DA4B4E, 0x003F81A9];
const EPIC: &[u32] = &[0x00DB5852, 0x00F5E569];
const FLAME_COLORS: &[u32] = &[0x00f0000, 0x00ff5a00, 0x00ff9a00, 0x00ffce00, 0x00ffe808];
//const LAVA_COLORS: &[u32] = &[
//    0x00ff5a00, 0x00ff5a00, 0x00ff5a00, 0x00ff5a00, 0x00ff5a00, 0x00ff5a00, 0x00ffce00,
//];
const WATER_COLORS: &[u32] = &[0x001C2E47, 0x00367B8C, 0x007FBABB, 0x00A7BBB5, 0x00B3CFC9];
const GREENS_N_GRAYS: &[u32] = &[0x00283033, 0x004C6D65, 0x0079AE93, 0x00C7DA96, 0x00F3F5D2];
const DIRT: &[u32] = &[0x00593d29, 0x00966c4a, 0x0079553a];
//const MANA: &[u32] = &[0x00dfb1bb, 0x00c298a6, 0x007d576c, 0x00634464, 0x00413156];
//const MANA: &[u32] = &[0x00b385dc, 0x00d0b6eb, 0x00c7effb, 0x00a8e2dc, 0x009cd1d0];
const MANA: &[u32] = &[0x00b8ffb9, 0x0093eac4, 0x0075dbd8, 0x006dc5df, 0x007eaee3];

const SCALE: usize = 8;
const WIDTH: usize = 200;
const HEIGHT: usize = 100;

macro_rules! sub_array {
    ($for: ident -> $item: ident [$field: tt]) => {
        impl Index<usize> for $for {
            type Output = $item;
            fn index(&self, i: usize) -> &$item {
                &self.$field[i]
            }
        }

        impl IndexMut<usize> for $for {
            fn index_mut(&mut self, i: usize) -> &mut $item {
                &mut self.$field[i]
            }
        }
    };

    ($for: ident <$T: ident: $($T_: tt ),* > -> [$field: ident]) => {
        impl<$T: $($T_ +)*> Index<usize> for $for<$T> {
            type Output = $T;
            fn index(&self, i: usize) -> &T {
                &self.$field[i]
            }
        }

        impl<$T: $($T_ +)*> IndexMut<usize> for $for<$T> {
            fn index_mut(&mut self, i: usize) -> &mut T {
                &mut self.$field[i]
            }
        }
    };
}

#[derive(Clone, Copy, Debug, PartialEq)]
struct Vec2d(f64, f64);

#[macro_export]
macro_rules! impl_op {
    (impl $trait: ident fn $fn: ident => $op: tt) => {
        impl $trait<f64> for Vec2d {
            type Output = Vec2d;
            fn $fn(self, other: f64) -> Vec2d {
                Vec2d(self.0 $op other, self.1 $op other)
            }
        }
        impl $trait<Vec2d> for f64 {
            type Output = Vec2d;
            fn $fn(self, other: Vec2d) -> Vec2d {
                Vec2d(self $op other.0, self $op other.1)
            }
        }
        impl $trait<Vec2d> for Vec2d {
            type Output = Vec2d;
            fn $fn(self, other: Vec2d) -> Vec2d {
                Vec2d(self.0 $op other.0, self.1 $op other.1)
            }
        }
    };
}

impl_op!(impl Mul fn mul => *);
impl_op!(impl Add fn add => +);
impl_op!(impl Div fn div => /);
impl_op!(impl Sub fn sub => -);
impl_op!(impl Rem fn rem => %);

impl Vec2d {
    fn index(&self, width: usize) -> usize {
        (self.0 + (self.1 * width as f64)) as usize
    }
    fn random_scale(min: f64, max: f64) -> Vec2d {
        Vec2d(
            thread_rng().gen_range(min, max),
            thread_rng().gen_range(min, max),
        )
    }
}

const VZERO: Vec2d = Vec2d(0.0, 0.0);

#[macro_export]
macro_rules! math2d{
    (|$($x: ident),*| $math: expr) => {{
        let f = |$($x,)* $(,)? |$math;
        Vec2d(f($($x.0,)*), f($($x.1,)*))
    }};
    ($res: expr => |$($x: ident),* $(,)? | $math: expr) => {{
        let f = |$($x,)*|$math;
        $res.0 = f($($x.0,)*);
        $res.1 = f($($x.1,)*);
        $res
    }};
    ($res: expr => |$($x: expr),* $(,)? => $($y: ident),* $(,)? | $math: expr) => {{
        let f = |$($y,)*|$math;
        $res.0 = f($($x.0,)*);
        $res.1 = f($($x.1,)*);
        $res
    }}
}

#[derive(Clone, Debug)]
struct ParticleBuffer<T: Clone> {
    data: Vec<T>,
    index: usize,
    is_full: bool,
    size: usize,
}

sub_array!(ParticleBuffer<T: Clone> -> [data]);

macro_rules! pb{
    ( $( $val: expr ),* $(,)?; $size: expr ) => {{
        let mut tmp = ParticleBuffer::new($size);
        $(
            tmp.push($val);
        )*
        tmp
    }};
    ($size: expr ) => {{
        ParticleBuffer::new($size)
    }}

}

impl<T: Clone> ParticleBuffer<T> {
    fn new(size: usize) -> ParticleBuffer<T> {
        ParticleBuffer {
            data: vec![],
            index: 0,
            is_full: false,
            size: size,
        }
    }
    fn push(&mut self, item: T) {
        self.index += 1;
        if self.index >= self.size {
            self.index = 0;
            self.is_full = true
        }
        if self.index >= self.data.len() {
            self.data.push(item)
        } else {
            self.data[self.index] = item
        }
    }
    fn len(&self) -> usize {
        self.data.len()
    }
    fn clear(&mut self) {
        *self = Self::new(self.size)
    }
}

#[derive(Clone, Copy, Debug)]
struct Square {
    a: Vec2d,
    b: Vec2d,
}

impl Square {
    fn draw(&self, buffer: &mut [u32], colort: Texture, width: &usize) {
        match colort {
            Texture::Color(color) => {
                let len = buffer.len() - 1;
                for y in self.a.1 as usize..self.b.1 as usize {
                    for p in &mut buffer[Vec2d(self.a.0, y as f64).index(*width).min(len)
                        ..Vec2d(self.b.0, y as f64).index(*width).min(len)]
                    {
                        *p = color
                    }
                }
            }
            Texture::Noise(color, seed) => {
                let mut rng = rand_pcg::Pcg32::seed_from_u64(seed);
                let len = buffer.len() - 1;
                for y in self.a.1 as usize..self.b.1 as usize {
                    for p in &mut buffer[Vec2d(self.a.0, y as f64).index(*width).min(len)
                        ..Vec2d(self.b.0, y as f64).index(*width).min(len)]
                    {
                        *p = *color.choose(&mut rng).unwrap()
                    }
                }
            }
            Texture::Grass(color, top_c, seed) => {
                let mut rng = rand_pcg::Pcg32::seed_from_u64(seed);
                let len = buffer.len() - 1;
                let mut top = true;
                for y in self.a.1 as usize..self.b.1 as usize {
                    for p in &mut buffer[Vec2d(self.a.0, y as f64).index(*width).min(len)
                        ..Vec2d(self.b.0, y as f64).index(*width).min(len)]
                    {
                        *p = if top {
                            *top_c.choose(&mut rng).unwrap()
                        } else {
                            *color.choose(&mut rng).unwrap()
                        }
                    }
                    top = false;
                }
            }
            Texture::NoiseAnimated(color) => {
                let len = buffer.len() - 1;
                for y in self.a.1 as usize..self.b.1 as usize {
                    for p in &mut buffer[Vec2d(self.a.0, y as f64).index(*width).min(len)
                        ..Vec2d(self.b.0, y as f64).index(*width).min(len)]
                    {
                        *p = *color.choose(&mut thread_rng()).unwrap()
                    }
                }
            }
        }
    }

    fn inside(&self, p: Vec2d) -> bool {
        p.0 > self.a.0 && p.1 > self.a.1 && p.0 < self.b.0 && p.1 < self.b.1
    }

    fn is_colliding(&self, other: &Square) -> bool {
        //let other_b = &other.p + &other.size;
        self.inside(other.a)
            || self.inside(other.b)
            || other.inside(self.a)
            || other.inside(self.b)
            || self.inside(Vec2d(other.a.0, other.b.1))
            || self.inside(Vec2d(other.b.0, other.a.1))
            || other.inside(Vec2d(self.a.0, self.b.1))
            || other.inside(Vec2d(self.b.0, self.a.1))
    }

    /*fn colliding_where(&self, other: &Self) -> Option<Square> {
        if self.is_colliding(other) {
            Some(*self)
        } else {
            None
        }
    }*/

    /*fn expand(mut self, n: f64) -> Self {
        self.a = self.a - n;
        self.b = self.b + n;
        self
    }*/
    /*fn draw_noise_texture(&self, buffer: &mut [u32], color: &[u32], width: &usize) {
        let len = buffer.len() - 1;
        for y in self.a.1 as usize..self.b.1 as usize {
            for p in &mut buffer[Vec2d(self.a.0, y as f64).index(*width).min(len)
                ..Vec2d(self.b.0, y as f64).index(*width).min(len)]
            {
                *p = *color.choose(&mut thread_rng()).unwrap()
            }
        }
    }*/
}

#[derive(Clone, Debug)]
struct Player {
    p: Vec2d,
    hp: usize,
    vel: Vec2d,
    dir: Vec2d,
    color: Texture,
}

impl Player {
    fn draw_box(&self, buffer: &mut [u32], width: &usize) {
        self.hitbox().draw(buffer, self.color, width)
    }
    fn hitbox(&self) -> Square {
        Square {
            a: self.p,
            b: self.p + Vec2d(2.0, 3.0),
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
enum Texture {
    Color(u32),
    Noise(&'static [u32], u64),
    NoiseAnimated(&'static [u32]),
    Grass(&'static [u32], &'static [u32], u64),
}

impl Texture {
    fn noise(&self) -> &[u32] {
        match self {
            Self::Color(_) => panic!("Cannot convert color to noise"),
            Self::Noise(c, _) => c,
            Self::NoiseAnimated(c) => c,
            Self::Grass(c, _, _) => c,
        }
    }
    fn color(&self) -> u32 {
        match self {
            Self::Color(c) => *c,
            Self::Noise(c, _) => c[0],
            Self::NoiseAnimated(c) => c[0],
            Self::Grass(c, _, _) => c[0],
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
struct PolySquare {
    p: Vec2d,
    size: Vec2d,
    vel: Vec2d,
    color: Texture,
}

impl PolySquare {
    /*fn center(&self) -> Vec2d {
        self.p + (self.size / 2.0)
    }*/
    fn is_out_of_bounds(&self, dim: (&usize, &usize)) -> bool {
        self.p.0 > *dim.0 as f64 || self.p.1 > *dim.1 as f64 || self.p.1 < 0.0 || self.p.0 < 0.0
    }
    fn draw(&self, buffer: &mut [u32], width: &usize) {
        self.hitbox().draw(buffer, self.color, width)
    }
    fn hitbox(&self) -> Square {
        Square {
            a: self.p,
            b: self.p + self.size,
        }
    }
    fn inside(&self, p: Vec2d) -> bool {
        let b = self.p + self.size;
        p.0 > self.p.0 && p.1 > self.p.1 && p.0 < b.0 && p.1 < b.1
    }

    fn is_colliding(&self, other: &Square) -> bool {
        let self_b = self.p + self.size;
        //let other_b = &other.p + &other.size;
        self.inside(other.a)
            || self.inside(other.b)
            || other.inside(self.p)
            || other.inside(self_b)
            || self.inside(Vec2d(other.a.0, other.b.1))
            || self.inside(Vec2d(other.b.0, other.a.1))
            || other.inside(Vec2d(self.p.0, self_b.1))
            || other.inside(Vec2d(self_b.0, self.p.1))
    }

    fn colliding_where(&self, other: &Square) -> Option<Square> {
        if self.is_colliding(other) {
            Some(self.hitbox())
        } else {
            None
        }
    }

    fn from_square(sq: &Square, color: Texture, vel: Vec2d) -> PolySquare {
        PolySquare {
            color: color,
            vel: vel,
            p: sq.a.clone(),
            size: sq.b - sq.a,
        }
    }
}

impl ParticleBuffer<PolySquare> {
    /*fn is_colliding(&self, other: &Square) -> bool {
        self.data.iter().any(|x| x.is_colliding(&other))
    }*/
    fn colliding_where(&self, other: &Square) -> Option<Square> {
        for block in &self.data {
            if block.is_colliding(&other) {
                return Some(block.hitbox());
            }
        }
        None
    }
    fn draw_n_shit(&mut self, buffer: &mut Vec<u32>, dim: (&usize, &usize)) {
        let mut n = 0;
        while n < self.data.len() {
            let par = &mut self.data[n];
            par.draw(buffer, dim.0);
            par.p = par.p + par.vel;
            if self.data[n].is_out_of_bounds(dim) {
                self.data.remove(n);
            } else {
                n += 1;
            }
        }
    }
}

/*
impl ParticleBuffer<ParticleBuffer<PolySquare>> {
    fn draw_n_shit(&mut self, buffer: &mut Vec<u32>, dim: (&usize, &usize)) {
        for n in &mut self.data {
            n.draw_n_shit(buffer, dim)
        }
    }
}
*/

macro_rules! cut {
    ($self: expr, $inner: expr, $buffer: expr) => {{
        let outer = $self.hitbox();
        for ps in [
            Square {
                // left
                a: outer.a,
                b: Vec2d($inner.a.0, outer.b.1),
            },
            Square {
                // right
                a: Vec2d($inner.b.0, outer.a.1),
                b: outer.b,
            },
            Square {
                // top
                a: outer.a,
                b: Vec2d(outer.b.0, $inner.a.1),
            },
            Square {
                // bttom
                a: Vec2d(outer.a.0, $inner.b.1),
                b: outer.b,
            },
        ]
        .iter()
        {
            if ps.a.0 < ps.b.0 && ps.a.1 < ps.b.1 {
                $buffer.push(PolySquare::from_square(ps, $self.color, $self.vel.clone()))
            }
        }
    }};
}

macro_rules! move_obj {
    ($obj: ident + $speed:expr => $up: ident $left: ident $down: ident $right: ident on $window: ident) => {{
        /*if $window.is_key_down(Key::$up) {
            $obj.p[1] -= $speed;
        }
        if $window.is_key_down(Key::$down) {
            $obj.p[1] += $speed;
        }*/
        if $window.is_key_down(Key::$left) {
            $obj.vel.0 = -$speed;
            $obj.dir.0 = -1.0;
        }
        if $window.is_key_down(Key::$right) {
            $obj.vel.0 = $speed;
            $obj.dir.0 = 1.0;
        }
    }};
}

macro_rules! jump {
    ($button: ident, $grav: expr; on $window: expr) => {{
        if $window.is_key_down(Key::$button) {
            $grav = -6.0;
        }
    }};
}

macro_rules! if_tt {
    (($($cond:tt)+) ($($yes:tt)*)) => {$($yes)*};
    ((            ) ($($yes:tt)*)) => {};
}

macro_rules! gravity {
    ( $(&$ref:tt)? $obj: ident, [$($ground: expr),* $(,)?] , $window: expr $(, on_collide($($collide_arg:expr),* $(,)? $(; $($col_ref:tt)? collider)?) = $collide_fn: expr)? $(, player $player: tt)? $(,)?) => {{
        let new_hitbox = {
            let mut new_p = $obj.clone();
            new_p.p = new_p.p + $obj.vel;
            new_p.hitbox()
        };

        let mut can_move = true;
        let _collider = Mutex::new(Square{a: VZERO,b: VZERO});
        $(if can_move{
            can_move = $ground.par_iter().all(|x: &_|
                if let Some(collider) = x.colliding_where(&new_hitbox){
                    *_collider.lock().unwrap()=collider;
                    false
                }else{
                    true
                });
        })*
        let _collider = _collider.lock().unwrap().clone();

        if can_move{
            $obj.p=$obj.p+$obj.vel
        }else{
            math2d!($obj.vel => |$obj.vel => vel| (vel)*-1.0);
            $($collide_fn($($collide_arg),* $(,$($col_ref)? _collider )? );)?
            if_tt!(($($player)?) (
                jump!(Space,$obj.vel.1; on $window);
                if $window.is_key_down(Key::A)
                || $window.is_key_down(Key::D)
                || $window.is_key_down(Key::K){
                    $obj.vel.1-=2.0
                }
                if _collider.is_colliding(&$obj.hitbox()){
                    $obj.p=$obj.p+$obj.vel
                }
            ));
        }

        if $obj.vel.1 < 10.0{
            $obj.vel.1 += 1.0
        }
        //$obj.vel = &$obj.vel/2.0;
        const SLOW: f64 = 0.5;
        math2d!($obj.vel => |$obj.vel => vel| if vel > SLOW{vel-SLOW} else if vel < -SLOW {vel+SLOW}else{vel} );
    }};
}

macro_rules! shoot_liquid {
    ($pb: expr, $type:expr, $spawn: expr, $dir: expr $(,)?) => {{
        $pb.push(match stringify!($type) {
            "fire" => PolySquare {
                p: $spawn,
                vel: $dir,
                size: Vec2d::random_scale(1.0, 3.0),
                color: Texture::Color(*FLAME_COLORS.choose(&mut thread_rng()).unwrap()),
            },
            "water" => PolySquare {
                p: $spawn,
                vel: $dir,
                size: Vec2d::random_scale(1.0, 3.0),
                color: Texture::Color(*WATER_COLORS.choose(&mut thread_rng()).unwrap()),
            },
            "steam" => PolySquare {
                p: $spawn,
                vel: $dir,
                size: Vec2d::random_scale(1.0, 3.0),
                color: Texture::Color(0x00898990),
            },
            "plant" => PolySquare {
                p: $spawn,
                vel: $dir,
                size: Vec2d::random_scale(1.0, 3.0),
                color: Texture::Color(COLORS[0]),
            },
            _ => PolySquare {
                p: $spawn,
                vel: $dir,
                size: Vec2d::random_scale(1.0, 3.0),
                color: Texture::Color(*EPIC.choose(&mut thread_rng()).unwrap()),
            },
        });
    }};
}

/*
macro_rules! flame_thrower {
    ($type:tt $pb: expr => $player:ident on $wrld: expr; with $buffer:expr, $window:ident; dim = ($width: expr, $height: expr); $bedrock:expr; key = $key: tt) => {{
        {
            let mut n = 0;
            while n < $pb.data.len() {
                $pb.data[n].draw(&mut $buffer, &$width);
                if $pb.data[n].p[0] > $width as f64
                    || $pb.data[n].p[1] > $height as f64
                    || $pb.data[n].p[1] < 0.0
                    || $pb.data[n].p[0] < 0.0
                {
                    $pb.data.remove(n);
                } else {
                    n += 1;
                }
            }
        }

        'pb: for n in 0..$pb.len() {
            let mut stop_me = false;

            let hitbox = $pb.data[n].hitbox();

            $wrld.iter_mut().for_each(|block| {
                'pb: for g in 0..block.len() {
                    if g < block.len() {
                        if block[g].inside(&hitbox.a) && block[g].inside(&hitbox.b) {
                            let e = block.data.remove(g);
                            cut!(&e, &hitbox, block);
                            $pb.data[n].vel = &$pb.data[n].vel / 2.0;
                            if stringify!($type) != "lava" {
                                $pb.data.remove(n);
                                stop_me = true;
                                break 'pb;
                            }
                        } else if {
                            let hit = block[g].hitbox();
                            hitbox.inside(&hit.a) && hitbox.inside(&hit.b)
                        } {
                            block.data.remove(g);
                            $pb.data[n].vel = &$pb.data[n].vel / 2.0
                        }
                    }
                }
            });
            if stop_me {
                break 'pb;
            }

            let thing = &mut $pb.data[n];
            thing.p = &thing.p + &thing.vel;
            thing.vel[1] = thing.vel[1] + 0.02;
        }
    }};
}
*/

/*
macro_rules! bounds_check_loop {
    ($self: expr, $buffer: expr, $dim: expr => $do: expr) => {{
        let mut n = 0;
        while n < $self.data.len() {
            let par = &mut $self.data[n];
            $do(par);
            if $self.data[n].p[0] > *$dim.0 as f64
                || $self.data[n].p[1] > *$dim.1 as f64
                || $self.data[n].p[1] < 0.0
                || $self.data[n].p[0] < 0.0
            {
                $self.data.remove(n);
            } else {
                n += 1;
            }
        }
    }};
}

macro_rules! thrower {
    ($pb: expr, [$($wrld: expr),* $(,)?] => $do: expr, $update: expr ) => {{
        'pb: for n in 0..$pb.len() {
            let mut stop_me = false;
            let hitbox = $pb.data[n].hitbox();

            $(if !stop_me{
                $wrld.iter_mut().for_each(|block| {
                    'me: for g in 0..block.len() {
                        if g < block.len() && !stop_me{
                            let stop_someone = $do(block, g, &hitbox, &$pb.data[n]);
                            if stop_someone.0 {
                                block.data.remove(g);
                            }
                            if stop_someone.1 {
                                $pb.data.remove(n);
                            }
                            if stop_someone.1 || stop_someone.0 {
                                stop_me = true;
                                break 'me
                            }
                        }
                    }
                });
            })*

            if !stop_me {
                if $update(&mut $pb.data[n]){
                    $pb.data.remove(n);
                    break 'pb;
                }
            }else{
                break 'pb
            }
        }
    }};
}*/

#[macro_export]
macro_rules! thrower {
    ($pb: expr $(,$field:ident)?, [$($wrld: expr),* $(,)?], $delete_small: expr $(,)? => $do: expr, $update: expr) => {{
        let delete_me = Mutex::new($pb.clone());
        $($wrld.par_iter_mut().for_each(|block| {
            let mut block_i = 0;
            while block_i < block.len() {
                let mut delete_block = false;
                let mut part_i = 0;

                let mut pb = delete_me.lock().unwrap();
                while part_i < pb.len() {
                    let info = $do(block, block_i, &pb.data[part_i]);
                    delete_block = delete_block || info.0;
                    if info.1 {
                        pb.data.remove(part_i);
                    } else {
                        part_i += 1;
                    }
                }

                if delete_block
                    || ($delete_small != 0.0 && block.data[block_i].size.0 < $delete_small)
                    || ($delete_small != 0.0 && block.data[block_i].size.1 < $delete_small)
                {
                    block.data.remove(block_i);
                } else {
                    block_i += 1;
                }
            }
        });)*
        $pb = delete_me.lock().unwrap().clone();
    }};
}

/*
macro_rules! rng_texture{
    ($( $type:ident $($c: ident+)* ),* $(,)?;$seed: expr) => {{
    let mut rng = rand_pcg::Pcg32::seed_from_u64($seed);
    *[$(Texture::$type($($c),*, $seed)),*]
        .choose(&mut rng)
        .unwrap()

    }}
}*/

fn ground(seed: u64) -> (Vec<ParticleBuffer<PolySquare>>, usize) {
    let mut rng = rand_pcg::Pcg32::seed_from_u64(seed);
    let mut env = Vec::new();
    //let color = rng_texture![Noise HILLS+, Noise DIRT+; seed];
    let color = Texture::Noise(HILLS, seed);
    let mut ground_plane = PolySquare {
        p: Vec2d(0.0, HEIGHT as f64 - 60.0),
        size: Vec2d(rng.gen_range(10.0, 20.0), 15.0),
        color: color,
        vel: VZERO,
    };
    while ground_plane.p.0 < WIDTH as f64 {
        env.push(pb![ground_plane.clone(); 40]);
        let mut new_p = Vec2d(
            ground_plane.p.0 + ground_plane.size.0,
            ground_plane.p.1 + rng.gen_range(-6.0, 6.0),
        );
        if new_p.1 > HEIGHT as f64 - 30.0 {
            new_p.1 = HEIGHT as f64 - 30.0;
        }
        ground_plane = PolySquare {
            p: new_p.clone(),
            size: Vec2d(rng.gen_range(10.0, 20.0), 15.0),
            color: color,
            vel: VZERO,
        };
        let width = ground_plane.p.0 + ground_plane.size.0;
        if width > WIDTH as f64 {
            ground_plane.size.0 -= width - WIDTH as f64;
        }
    }
    let env_len = env.len();
    let color = Texture::Noise(DIRT, seed);
    for n in 0..env.len() {
        let mut p = env[n].data[0].p.clone();
        p.1 += env[n].data[0].size.1;
        ground_plane = PolySquare {
            p: p.clone(),
            size: Vec2d(env[n].data[0].size.0, 20.0),
            color: color,
            vel: VZERO,
        };
        env.push(pb![ground_plane; 30]);
    }
    let color = Texture::Noise(ROCKS, seed);
    for n in 0..env_len {
        // - - - - - - - - - - - -
        let mut p = env[n].data[0].p.clone();
        p.1 += env[n].data[0].size.1 + 20.0;
        ground_plane = PolySquare {
            p: p.clone(),
            size: Vec2d(env[n].data[0].size.0, HEIGHT as f64 - p.1),
            color: color,
            vel: VZERO,
        };
        env.push(pb![ground_plane; 30]);
    }
    //env.push(pb![ground_plane.clone(); 40]);
    (env, env_len)
}

fn stars(seed: u64) -> Vec<usize> {
    let mut rng = rand_pcg::Pcg32::seed_from_u64(seed);
    let mut background = vec![];
    for _ in 0..100 {
        background.push(rng.gen_range(0, HEIGHT * WIDTH))
    }
    background
}

fn gen_rocks(seed: u64) -> ParticleBuffer<PolySquare> {
    let mut rocks = pb![10];
    let mut rng = rand_pcg::Pcg32::seed_from_u64(seed);
    for _ in 0..rocks.size {
        rocks.push(PolySquare {
            p: Vec2d(
                rng.gen_range(0.0, WIDTH as f64),
                rng.gen_range(0.0, HEIGHT as f64),
            ),
            size: Vec2d(rng.gen_range(3.0, 8.0), rng.gen_range(2.0, 8.0)),
            color: Texture::Noise(ROCKS, seed),
            vel: VZERO,
        });
    }
    rocks
}

fn gen_lava(seed: u64) -> (f64, ParticleBuffer<PolySquare>) {
    let mut rocks = pb![8];
    let mut rng = rand_pcg::Pcg32::seed_from_u64(seed);
    let base = Vec2d(
        rng.gen_range(0.0, WIDTH as f64),
        rng.gen_range(0.0, HEIGHT as f64),
    );

    for n in 0..rocks.size as u64 {
        rocks.push(PolySquare {
            p: Vec2d(
                base.0 + rng.gen_range(0.0, 10.0),
                base.1 + rng.gen_range(0.0, 10.0),
            ),
            size: Vec2d(rng.gen_range(1.0, 3.0), rng.gen_range(1.0, 3.0)),
            color: Texture::Noise(MANA, seed + n),
            vel: VZERO,
        });
    }
    let manap = ((rocks.data[0].p.1 - 25.0) * 2.0).max(5.0);
    println!("MANA = {}", manap);
    (manap, rocks)
}

macro_rules! rm_loop {
    ($thing: expr => $do: expr) => {{
        let mut n = 0;
        while n < $thing.len() {
            if $do(&mut $thing[n]) {
                $thing.remove(n);
            } else {
                n += 1;
            }
        }
    }};
}

macro_rules! art{
    ($($b: tt)*) => {{
        Box::new([
            $(if stringify!($b) == "#"{true}else if stringify!($b) == "."{false}else{panic!("PLEASE ONLY USE '#' OR '.' IN BIN")}),*
        ])
    }}
}

/*lazy_static! {
    static ref GAME_OVER: Box<[bool]> = bin![
        # . . . # . . . # # # . . . # # # . . . # # . . . # # # . .
        # . . . # . . . # . . # . . # . . . . # . . # . . # . . # .
        # . . . # . . . # . . . # . # . . . . # . . # . . # . . . #
        # . . . # . . . # . . . # . # # . . . # # # # . . # . . . #
        # . . . # . . . # . . . # . # . . . # # . . # # . # . . . #
        # . . . # . . . # . . . # . # . . . # . . . . # . # . . . #
        # . . . # . . . # . . # . . # . . . # . . . . # . # . . # .
        . # # # . . . . # # # . . . # # # . # . . . . # . # # # . .
    ];
}*/

/*lazy_static! {
    static ref GAME_OVER: Box<[bool]> = bin![
    . . # # # # # # . . . . . . # # # . . . . # # . . . . . # # . # # # # # # # # . . . . . . . . # # # # # # # . . # # . . . . . # # . # # # # # # # # . # # # # # # # # .
    . # # . . . . # # . . . . # # . # # . . . # # # . . . # # # . # # . . . . . . . . . . . . . # # . . . . . # # . # # . . . . . # # . # # . . . . . . . # # . . . . . # #
    . # # . . . . . . . . . # # . . . # # . . # # # # . # # # # . # # . . . . . . . . . . . . . # # . . . . . # # . # # . . . . . # # . # # . . . . . . . # # . . . . . # #
    . # # . . . # # # # . # # . . . . . # # . # # . # # # . # # . # # # # # # . . . . . . . . . # # . . . . . # # . # # . . . . . # # . # # # # # # . . . # # # # # # # # .
    . # # . . . . # # . . # # # # # # # # # . # # . . . . . # # . # # . . . . . . . . . . . . . # # . . . . . # # . . # # . . . # # . . # # . . . . . . . # # . . . # # . .
    . # # . . . . # # . . # # . . . . . # # . # # . . . . . # # . # # . . . . . . . . . . . . . # # . . . . . # # . . . # # . # # . . . # # . . . . . . . # # . . . . # # .
    . . # # # # # # . . . # # . . . . . # # . # # . . . . . # # . # # # # # # # # . . . . . . . . # # # # # # # . . . . . # # # . . . . # # # # # # # # . # # . . . . . # #
    ];
}*/

/*lazy_static! {
    static ref GAME_OVER: Box<[bool]> = art![
        . # # . . . . # # . . # # # # # # # . . # # . . . . . # # . . . . . . . # # # # # # # # . . # # # # . # # # # # # # # . # # # # # # # # .
        . . # # . . # # . . # # . . . . . # # . # # . . . . . # # . . . . . . . # # . . . . . # # . . # # . . # # . . . . . . . # # . . . . . # #
        . . . # # # # . . . # # . . . . . # # . # # . . . . . # # . . . . . . . # # . . . . . # # . . # # . . # # . . . . . . . # # . . . . . # #
        . . . . # # . . . . # # . . . . . # # . # # . . . . . # # . . . . . . . # # . . . . . # # . . # # . . # # # # # # . . . # # . . . . . # #
        . . . . # # . . . . # # . . . . . # # . # # . . . . . # # . . . . . . . # # . . . . . # # . . # # . . # # . . . . . . . # # . . . . . # #
        . . . . # # . . . . # # . . . . . # # . # # . . . . . # # . . . . . . . # # . . . . . # # . . # # . . # # . . . . . . . # # . . . . . # #
        . . . . # # . . . . . # # # # # # # . . . # # # # # # # . . . . . . . . # # # # # # # # . . # # # # . # # # # # # # # . # # # # # # # # .
        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . # . # # . . . . . # . . . . # # # . # . . . . # . . # # . . .
        . . . . # # . . # . # # # . . # # . # . # . # . . . . . . . . . . . . . . . # . . # . . . . . . . . . . . # . . . . . . . # . . # . . . .
        . . . . # . # . . . . # . . # . . . # . # . # . . . . . . . . . . . . . . . . . . # . . . . . . . . . . . . . . . . . . . . . . . . . . .
        . . . . # # . . # . . # . . # . . . # # # . # . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . # . . . . . . . . . . # . . . .
        . . . . # . # . # . . # . . # . . . # . # . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
        . . . . # # . . # . . # . . . # # . # . # . # . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    ];
}*/

lazy_static! {
    static ref GAME_OVER: Box<[bool]> = art![
        . # # . . . . # # . . # # # # # # # . . # # . . . . . # # . . . . . . . # # # # # # # # . . # # # # . # # # # # # # # . # # # # # # # # .
        . . # # . . # # . . # # . . . . . # # . # # . . . . . # # . . . . . . . # # . . . . . # # . . # # . . # # . . . . . . . # # . . . . . # #
        . . . # # # # . . . # # . . . . . # # . # # . . . . . # # . . . . . . . # # . . . . . # # . . # # . . # # . . . . . . . # # . . . . . # #
        . . . . # # . . . . # # . . . . . # # . # # . . . . . # # . . . . . . . # # . . . . . # # . . # # . . # # # # # # . . . # # . . . . . # #
        . . . . # # . . . . # # . . . . . # # . # # . . . . . # # . . . . . . . # # . . . . . # # . . # # . . # # . . . . . . . # # . . . . . # #
        . . . . # # . . . . # # . . . . . # # . # # . . . . . # # . . . . . . . # # . . . . . # # . . # # . . # # . . . . . . . # # . . . . . # #
        . . . . # # . . . . . # # # # # # # . . . # # # # # # # . . . . . . . . # # # # # # # # . . # # # # . # # # # # # # # . # # # # # # # # .
        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . # . # # . . . . . # . . . . # # # . # . . . . # . . # # . . .
        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . # . . # . . . . . . . . . . . # . . . . . . . # . . # . . . .
        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . # . . . . . . . . . . . . . . . . . . . . . . . . . . .
        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . # . . . . . . . . . . # . . . .
        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
        . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    ];
}

lazy_static! {
    static ref WIZARD: [Box<[bool]>; 4] = [
        art![
            . . . . . . # . . . # .
            . . . . . # . . . # # #
            . . . . # # # . . . # .
            . . # # # # # # # . . .
            . . . . # # # . . . # .
            . . . . # # # . . . # .
            . . . . . # . . . . # .
            . . . # # # # # # # # .
            . . # . # # # . . . # .
            . . # . # # # . . . # .
            . . # . # . # . . . # .
            . . . . # . # . . . # .
            . . . . # . # . . . # .
        ],
        art![
            . . . # . . . . . . . .
            . . . . # . . . . . . .
            . . . # # # . . . . . #
            . . # # # # # # . . # .
            . # . # # # . . . # # .
            . . . # # # . . . # . #
            . . . . # . . . # # # .
            . . # # # # # # . . . #
            . # . # # # . . . # . .
            . # . # # # . . . . . #
            . # . # . # . . . . . .
            . . . # . # . . . . . .
            . . . # . # . . . . . .
        ],
        art![
            . . . . . # . . . . . .
            . . . . # # # . . . . .
            . . . # # # # # . . . .
            . . # . # # # . # . . .
            . . . . # # # . . . . .
            . . . . . # . . . . . .
            . . . # # # # # . . . .
            . . # . # # # . # . . .
            . . # . # . # . # . . .
            . . . . # . # . . . . .
            . . . . # . # . . . . .
            . . . # . # . # . . . .
            . . # . # # . # # . # .
        ],
        art![
            . . . . . # . . . . . .
            . . . . # # # . . . . .
            . . . # # # # # . . . .
            . . # . # # # . # . . .
            . . . . # # # . . . . .
            . . . . . # . . . . . .
            . . . # # # # # . . . .
            . . # . # # # . # . . .
            # . # . # . # . # . # .
            . . . . # . # . . . . .
            . . . . # . # . . . # .
            . # . . . . . . # . # .
            . # . # # # # # # # # .
        ],
    ];
}

lazy_static! {
    static ref NO_WIZARD: Box<[bool]> = art![
        . # # # # # # # # # # .
        # . . . . . . . . . . #
        # . # . # . # . # . . #
        # . . # . # . # . # . #
        # . # . # . # . # . . #
        # . . # . # . # . # . #
        # . # . # . # . # . . #
        # . . # . # . # . # . #
        # . # . # . # . # . . #
        # . . # . # . # . # . #
        # . # . # . # . # . . #
        # . . . . . . . . . . #
        . # # # # # # # # # # .
    ];
}

fn game_over(buffer: &mut Vec<u32>, width: usize) {
    for x in 0..69 {
        for y in 0..13 {
            if (*GAME_OVER)[Vec2d(x as f64, y as f64).index(69)] {
                buffer[Vec2d((x + 20) as f64, (y + 10) as f64).index(width)] =
                    *BLOOD.choose(&mut thread_rng()).unwrap()
            }
        }
    }
}

fn draw_wiz(p: Vec2d, element: isize, buffer: &mut Vec<u32>, width: usize) {
    for x in 0..12 {
        for y in 0..13 {
            if if element == -1 {
                (*NO_WIZARD)[Vec2d(x as f64, y as f64).index(12)]
            } else {
                (*WIZARD)[element as usize][Vec2d(x as f64, y as f64).index(12)]
            } {
                buffer[Vec2d(x as f64 + p.0, y as f64 + p.1).index(width)] = *match element {
                    1 => FLAME_COLORS,
                    0 => WATER_COLORS,
                    2 => WATER_COLORS,
                    3 => GREENS_N_GRAYS,
                    _ => ROCKS,
                }
                .choose(&mut thread_rng())
                .unwrap()
            }
        }
    }
}

//elements

type ElementFn = fn(&mut ParticleBuffer<PolySquare>, usize, &PolySquare) -> (bool, bool);

static FIRE_FN: ElementFn =
    |block: &mut ParticleBuffer<PolySquare>, i: usize, part: &PolySquare| {
        let hitbox = part.clone().hitbox();
        if block[i].inside(hitbox.a) && block[i].inside(hitbox.b) {
            if random::<f64>() > 0.8 {
                cut!(&block[i].clone(), hitbox, block);
                (true, true)
            } else {
                (false, true)
            }
        } else if {
            let hit = block[i].hitbox();
            hitbox.inside(hit.a) && hitbox.inside(hit.b)
        } {
            (random(), true)
        } else {
            (false, false)
        }
    };

macro_rules! grav_attack {
    ($scale: expr) => {
        |block: &mut ParticleBuffer<PolySquare>, i: usize, part: &PolySquare| {
            let hitbox = part.clone().hitbox();
            if block[i].is_colliding(&hitbox) {
                for _ in 0..5 {
                    let hbox = block[i].hitbox();
                    let np = Vec2d(
                        thread_rng().gen_range(hbox.a.0, (hbox.b.0 - 8.0).max(hbox.a.0 + 2.0)),
                        thread_rng().gen_range(hbox.a.1, (hbox.b.1 - 8.0).max(hbox.a.1 + 2.0)),
                    );
                    let new_block = PolySquare {
                        p: np,
                        size: Vec2d::random_scale(6.0, 8.0),
                        color: block[i].color,
                        vel: Vec2d((random::<f64>() - 0.5) * 0.5, random::<f64>() * $scale),
                    };
                    block.push(new_block);
                }
                (true, true)
            } else {
                (false, false)
            }
        }
    };
}

//--------

fn main() {
    println!("E N D L E S S");
    let mut window = Window::new(
        "E N D L E S S - Press ESC to exit",
        WIDTH,
        HEIGHT,
        WindowOptions {
            scale: Scale::X8,
            ..WindowOptions::default()
        },
    )
    .unwrap();

    let mut enemys = pb![15];
    window.limit_update_rate(Some(std::time::Duration::from_micros(16600)));

    let mut flames = ParticleBuffer::<PolySquare>::new(25);
    let mut bullets = ParticleBuffer::<PolySquare>::new(25);
    let mut fx = ParticleBuffer::<PolySquare>::new(10);

    let mut area = random();
    let mut player_1 = Player {
        p: Vec2d(100.0, 0.0),
        hp: 10,
        vel: VZERO,
        dir: VZERO,
        color: Texture::Noise(MANA, area),
    };
    let (mut env, _) = ground(area);
    let mut background = stars(area);
    let mut amo = 50.0;
    let mut boost = 20.0;
    let mut rocks = gen_rocks(area);
    let mut lava = pb![10];
    let mut rng = rand_pcg::Pcg32::seed_from_u64(area);
    let mut lvl = 0;

    for _ in 0..rng.gen_range(0, 1) {
        for _ in 0..rng.gen_range(0, 10) {
            lava.push(gen_lava(area))
        }
    }
    let mut explored = (0, 0);

    let bedrock: PolySquare = PolySquare {
        p: Vec2d(0.0, HEIGHT as f64 - 5.0),
        size: Vec2d(WIDTH as f64, 5.0),
        color: Texture::Noise(ROCKS, area),
        vel: Vec2d(0.0, 0.0),
    };

    let mut element = "none";
    let mut element_i: isize = 0;
    const ELEMENTS: &[&'static str] = &["water", "fire", "steam", "gravity"];

    'game: while window.is_open() && !window.is_key_down(Key::Escape) {
        if player_1.p.1 < 0.0 {
            player_1.p.1 = 0.0
        }
        let (width, height) = {
            let mut tmp = window.get_size();
            tmp.0 = tmp.0 / SCALE;
            tmp.1 = tmp.1 / SCALE;
            tmp
        };
        let mut buffer = vec![0; width * height];
        for star in &background {
            buffer[*star] = 0x00292930
        }

        if player_1.hp < 1 && window.is_open() && !window.is_key_down(Key::Escape) {
            let (width, height) = {
                let mut tmp = window.get_size();
                tmp.0 = tmp.0 / SCALE;
                tmp.1 = tmp.1 / SCALE;
                tmp
            };
            let mut buffer = vec![0; width * height];
            for star in &background {
                buffer[*star] = 0x00292930
            }

            game_over(&mut buffer, width);
            bedrock.draw(&mut buffer, &(width));
            for block in &mut env {
                block.data.iter().for_each(|x| x.draw(&mut buffer, &width));
            }
            {
                let mut n = 0;
                while n < rocks.data.len() {
                    rocks.data[n].draw(&mut buffer, &width);
                    if rocks.data[n].p.0 > width as f64
                        || rocks.data[n].p.1 > height as f64
                        || rocks.data[n].p.1 < 0.0
                        || rocks.data[n].p.0 < 0.0
                    {
                        rocks.data.remove(n);
                    } else {
                        let rock = &mut rocks[n];
                        gravity!(&*rock, [env, [&bedrock]], window);
                        n += 1;
                    }
                }
            }
            window
                .update_with_buffer(&mut buffer, width, height)
                .unwrap();
            continue 'game;
        }

        if element == "none" && window.is_open() && !window.is_key_down(Key::Escape) {
            use KeyRepeat::*;
            if window.is_key_pressed(Key::Left, No) || window.is_key_pressed(Key::A, No) {
                element_i -= 1;
            }
            if window.is_key_pressed(Key::Right, No) || window.is_key_pressed(Key::D, No) {
                element_i += 1;
            }
            if element_i >= ELEMENTS.len() as isize {
                element_i = 0;
            }
            if element_i < 0 {
                element_i = ELEMENTS.len() as isize - 1
            }
            if window.is_key_pressed(Key::Space, No) || window.is_key_pressed(Key::Enter, No) {
                element = ELEMENTS[element_i as usize]
            }
            for n in 0..ELEMENTS.len() {
                draw_wiz(
                    Vec2d(n as f64 * 20.0 + 20.0, 50.0),
                    if n == element_i as usize {
                        element_i
                    } else {
                        -1
                    },
                    &mut buffer,
                    width,
                )
            }
            window
                .update_with_buffer(&mut buffer, width, height)
                .unwrap();
            continue 'game;
        }

        bedrock.draw(&mut buffer, &(width));
        for block in &mut env {
            block.data.iter_mut().for_each(|x| {
                x.p = x.p + x.vel;
                x.vel = x.vel * 0.95;
                x.draw(&mut buffer, &width)
            });
        }
        for n in 0..player_1.hp {
            buffer[n.min(width)] = COLORS[3];
            buffer[Vec2d(n.min(width) as f64, 1.0).index(width)] = COLORS[3];
        }
        buffer[10] = EPIC[1];
        buffer[Vec2d(10.0, 1.0).index(width)] = EPIC[1];

        for n in 0..amo as usize {
            buffer[Vec2d(n.min(width) as f64, 2.0).index(width)] = COLORS[4];
            buffer[Vec2d(n.min(width) as f64, 3.0).index(width)] = COLORS[4];
        }
        for n in 0..boost as usize {
            buffer[Vec2d(n.min(width) as f64, 4.0).index(width)] = GREENS_N_GRAYS[0];
            buffer[Vec2d(n.min(width) as f64, 5.0).index(width)] = GREENS_N_GRAYS[0];
        }

        {
            let hp = Mutex::new(player_1.hp);
            let vel = Mutex::new(player_1.vel.clone());
            let buf = Mutex::new(buffer.clone());
            let boost_ = Mutex::new(boost);
            enemys.data.par_iter_mut().for_each(|enemy: &mut Player| {
                if enemy.hp <= 0{
                    enemy.hitbox().draw(&mut *buf.lock().unwrap(),Texture::NoiseAnimated(BLOOD),&width);
                    return;
                }
                if enemy.hitbox().is_colliding(&player_1.hitbox()) {
                    {
                        let mut hp = hp.lock().unwrap();
                        if *hp > 0{
                            *hp -= 1;
                        }
                    }
                    math2d!(vel.lock().unwrap() => | player_1.vel, player_1.p, enemy.p => vel, p1, bad|
                        vel + if (p1-bad) == 0.0{2.0}else{p1-bad} * 2.0
                    );
                }
                for bullet in &flames.data{
                    if enemy.hitbox().is_colliding(&bullet.hitbox()){
                        if enemy.hp > 0{
                            enemy.hp -= 1;
                            math2d!(enemy.vel => | enemy.vel, enemy.p, bullet.p => vel, p1, bad|
                                vel + if (p1-bad) == 0.0{1.0}else{p1-bad}
                            );
                        }
                        if enemy.hp <= 0{
                            *boost_.lock().unwrap() = 20.0;
                            *hp.lock().unwrap() += 2;
                        }
                    }
                }
                if enemy.p.0 > player_1.p.0 {
                    enemy.vel.0 = -0.4;
                    enemy.dir.0 = -1.0;
                } else {
                    enemy.vel.0 = 0.4;
                    enemy.dir.0 = 1.0;
                }
                enemy.draw_box(&mut *buf.lock().unwrap(), &width);
                gravity!(
                    &*enemy,
                    [env, [&bedrock], rocks.data],
                    window,
                    on_collide(enemy; & collider) = |enemy: &mut Player, _collider: &Square| {
                        let hitbox = enemy.hitbox();
                if _collider.inside(hitbox.a) || _collider.inside(hitbox.b){
                    enemy.hp=0;
                }
                        enemy.vel.1 = -4.0
                    }
                );
            });
            player_1.hp = *hp.lock().unwrap();
            player_1.vel = vel.lock().unwrap().clone();
            buffer = buf.lock().unwrap().clone();
            boost = *boost_.lock().unwrap();
        }

        move_obj!(player_1 + 0.5 => W A S D on window);

        match element_i {
            0 => {
                // --- WATER ---
                if window.is_key_down(Key::L) && amo > 0.0 {
                    shoot_liquid!(
                        flames,
                        water,
                        player_1.p.clone(),
                        Vec2d(
                            random::<f64>() * 6.0 * player_1.dir.0,
                            random::<f64>() * -6.0
                        )
                    );
                    amo -= 0.2;
                }

                if window.is_key_down(Key::M) && amo > 0.0 {
                    for _ in 0..3 {
                        shoot_liquid!(
                            flames,
                            water,
                            player_1.p.clone(),
                            Vec2d(
                                (random::<f64>() - 0.5) * 6.0,
                                (random::<f64>() - 0.5) * 12.0,
                            )
                        );
                    }
                    amo -= 1.0;
                }

                flames
                    .data
                    .par_iter_mut()
                    .for_each(|bullet| gravity!(&*bullet, [env, [&bedrock]], window));
                rm_loop!(flames.data => |flames: &mut PolySquare|{
                flames.draw(&mut buffer, &width);
                flames.is_out_of_bounds((&width, &height))
                });

                if window.is_key_down(Key::K) && boost > 0.0 {
                    player_1.vel.0 = player_1.dir.0 * 5.0;
                    shoot_liquid!(
                        fx,
                        water,
                        player_1.p.clone(),
                        Vec2d(player_1.dir.0 * -1.0, random::<f64>() - 0.5)
                    );
                    boost -= 1.0;
                } else {
                    if boost < 20.0 {
                        boost += 0.1;
                    }
                }
            }
            1 => {
                // --- FIRE ---
                if window.is_key_down(Key::L) && amo > 0.0 {
                    shoot_liquid!(
                        flames,
                        fire,
                        player_1.p.clone(),
                        Vec2d(
                            (random::<f64>() + 0.5) * 0.5 * player_1.dir.0,
                            (random::<f64>() / 2.0) - 1.0,
                        )
                    );
                    amo -= 0.2;
                }

                if window.is_key_down(Key::M) && amo > 0.0 {
                    for _ in 0..3 {
                        shoot_liquid!(
                            flames,
                            fire,
                            player_1.p.clone(),
                            Vec2d((random::<f64>() - 0.5) * 3.0, (random::<f64>() - 0.5) * 3.0,)
                        );
                    }
                    amo -= 1.0;
                }

                if window.is_key_down(Key::K) && boost > 0.0 {
                    player_1.vel.0 = player_1.dir.0 * 5.0;
                    shoot_liquid!(
                        flames,
                        fire,
                        player_1.p.clone(),
                        Vec2d(player_1.dir.0 * -1.0, random::<f64>() - 0.5)
                    );
                    boost -= 1.0;
                } else {
                    if boost < 20.0 {
                        boost += 0.3;
                    }
                }

                thrower!(flames, [env], 6.0, => FIRE_FN, ||{});
                rm_loop!(flames.data => |flames: &mut PolySquare|{
                    flames.draw(&mut buffer, &width);
                    flames.p = flames.p + flames.vel;
                    flames.vel.1 += 0.05;
                    flames.is_out_of_bounds((&width, &height))
                });
            }
            2 => {
                // --- STEAM ---
                if window.is_key_down(Key::L) && amo > 0.0 {
                    shoot_liquid!(
                        flames,
                        water,
                        player_1.p.clone(),
                        Vec2d(
                            (random::<f64>() + 0.5) * 0.5 * player_1.dir.0,
                            (random::<f64>() / 2.0) - 1.0,
                        )
                    );
                    amo -= 0.2;
                }

                if window.is_key_down(Key::M) && amo > 0.0 {
                    for _ in 0..3 {
                        shoot_liquid!(
                            flames,
                            water,
                            player_1.p.clone(),
                            Vec2d((random::<f64>() - 0.5) * 3.0, (random::<f64>() - 0.5) * 3.0,)
                        );
                    }
                    amo -= 1.0;
                }

                if window.is_key_down(Key::K) && boost > 0.0 {
                    player_1.vel.1 -= 0.7;
                    shoot_liquid!(
                        fx,
                        water,
                        player_1.p.clone(),
                        Vec2d(random::<f64>() - 0.5, 2.0)
                    );
                    boost -= 0.1;
                } else {
                    if boost < 20.0 {
                        boost += 0.1;
                    }
                }

                thrower!(flames, [env], 6.0, => FIRE_FN, ||{});
                rm_loop!(flames.data => |flames: &mut PolySquare|{
                    flames.draw(&mut buffer, &width);
                    flames.p = flames.p + flames.vel;
                    flames.vel.1 += 0.05;
                    flames.is_out_of_bounds((&width, &height))
                });
            }

            3 => {
                // --- GRAVITY ---
                if window.is_key_down(Key::L) && amo > 0.0 {
                    shoot_liquid!(
                        flames,
                        water,
                        player_1.p.clone(),
                        Vec2d(
                            (random::<f64>() + 0.5) * 0.5 * player_1.dir.0,
                            (random::<f64>() / 2.0) - 1.0,
                        )
                    );
                    amo -= 0.2;
                }
                if window.is_key_down(Key::M) && amo > 0.0 {
                    shoot_liquid!(
                        bullets,
                        water,
                        player_1.p.clone(),
                        Vec2d(
                            (random::<f64>() + 0.5) * 0.5 * player_1.dir.0,
                            (random::<f64>() / 2.0) - 1.0,
                        )
                    );
                    amo -= 0.2;
                }

                if window.is_key_down(Key::K) && boost > 0.0 {
                    player_1.vel.1 -= 0.6;
                    boost -= 0.2;
                } else {
                    if boost < 20.0 {
                        boost += 0.1;
                    }
                }

                thrower!(flames, [env, [&mut rocks]], 6.0, => grav_attack!(-1.5), ||{});
                thrower!(bullets, [env, [&mut rocks]], 6.0, => grav_attack!(1.5), ||{});
                rm_loop!(flames.data => |flames: &mut PolySquare|{
                    flames.draw(&mut buffer, &width);
                    flames.p = flames.p + flames.vel;
                    flames.vel.1 += 0.05;
                    flames.is_out_of_bounds((&width, &height))
                });
                rm_loop!(bullets.data => |flames: &mut PolySquare|{
                    flames.draw(&mut buffer, &width);
                    flames.p = flames.p + flames.vel;
                    flames.vel.1 += 0.05;
                    flames.is_out_of_bounds((&width, &height))
                });
            }

            _ => panic!("UNKNOWN ELEMENTAL"),
        }

        player_1.draw_box(&mut buffer, &width);
        gravity!(player_1, [env, [&bedrock], rocks.data], window, player .);

        fx.draw_n_shit(&mut buffer, (&width, &height));
        {
            let mut n = 0;
            while n < rocks.data.len() {
                rocks.data[n].draw(&mut buffer, &width);
                if rocks.data[n].p.0 > width as f64
                    || rocks.data[n].p.1 > height as f64
                    || rocks.data[n].p.1 < 0.0
                    || rocks.data[n].p.0 < 0.0
                {
                    rocks.data.remove(n);
                } else {
                    let rock = &mut rocks[n];
                    gravity!(&*rock, [env, [&bedrock]], window);
                    n += 1;
                }
            }
        }
        {
            let mut n = 0;
            while n < lava.data.len() {
                let mut kill_me = false;
                let xamo = lava[n].0;
                'alava: for alava in &mut lava[n].1.data {
                    gravity!(&*alava, [env, rocks.data, [&bedrock]], window);
                    alava.draw(&mut buffer, &width);
                    if alava.p.0 > width as f64
                        || alava.p.1 > height as f64
                        || alava.p.1 < 0.0
                        || alava.p.0 < 0.0
                    {
                        kill_me = true;
                    }
                    if alava.is_colliding(&player_1.hitbox()) {
                        kill_me = true;
                        amo += xamo;
                        break 'alava;
                    }
                }
                if kill_me {
                    lava.data.remove(n);
                } else {
                    n += 1;
                }
            }
        }

        if player_1.p.0 > width as f64 || player_1.p.0 < 0.0 {
            flames.clear();
            lava.clear();
            enemys.clear();
            if player_1.p.0 < 0.0 {
                area -= 1
            } else {
                area += 1
            };
            println!("area = {}, lvl = {}", area, lvl);
            let mut rng = rand_pcg::Pcg32::seed_from_u64(area);
            if area > explored.0 {
                if thread_rng().gen() {
                    lvl += 1
                }
                explored.0 = area;
                if rng.gen::<f64>() < 0.8 {
                    for n in 0..rng.gen_range(0, 3) {
                        lava.push(gen_lava(area + n))
                    }
                }
            }
            if area < explored.1 {
                if thread_rng().gen() {
                    lvl += 1
                }
                explored.1 = area;
                if rng.gen::<f64>() < 0.8 {
                    for n in 0..rng.gen_range(0, 3) {
                        lava.push(gen_lava(area + n))
                    }
                }
            }
            let mut rng = rand_pcg::Pcg32::seed_from_u64(area);
            if rng.gen() {
                for n in 0..(lvl + rng.gen::<bool>() as usize).min(enemys.size) as u64 {
                    enemys.push(Player {
                        p: Vec2d(rng.gen::<f64>() * WIDTH as f64, 0.0),
                        hp: 5,
                        vel: VZERO,
                        dir: VZERO,
                        color: Texture::Noise(GREENS_N_GRAYS, area + n),
                    });
                }
            }

            let tmp = ground(area);
            env = tmp.0;
            if player_1.p.0 < 0.0 {
                player_1.p.1 = env[tmp.1 - 1].data[0].p.1 - 10.0;
                player_1.p.0 = width as f64;
            } else {
                player_1.p.0 = 0.0;
                player_1.p.1 = env[0].data[0].p.1 - 10.0;
            }
            background = stars(area);
            rocks = gen_rocks(area);
        }
        if player_1.p.1 > height as f64 {
            player_1.p.1 = 0.0;
        }

        window
            .update_with_buffer(&buffer[..], width, height)
            .unwrap();
    }
}
