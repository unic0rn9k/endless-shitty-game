# ABOUT
I wanted to hold a little game jam for myself, so i wrote this small game (+game engine) using rust and minfb. It took about a week (plus an extra day for polish).
# HOW TO PLAY
You sellect a charecter in the begin with either the arrow keys or with **'A'** and **'D'**. After that you can walk with **'A'** and **'D'**, you shoot with **'L'**, which uses mana (the **blue** bar). And you can use a special on **'M'**, which uses extra mana. You kan use a movement thing on **'K'**, which takes up stamina (the gray bar). When you kill zombies (the small jumpy green dudes) you gain hp (the **red** bar)

The light blue and green stuff (with same colros as the player) on the ground are mana pickups. They spawn in the ground too, so it is possible to shoot the ground and dig down to them.
# KNOWN BUGS
 - Full window does not work at all :/
 - Inverted controlles when stuck in object
 - Under some wayland compositors (wayfire atleast) game crashes if mouse is hovering over window.
